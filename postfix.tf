provider "azurerm" {
}

variable "dnsname" {
    default = "testpostfix"
}


# Create Resource Group
resource "azurerm_resource_group" "rg" {
        name = "DEV_OPS_TEST"
        location = "westeurope"
}

# Create Storage Account
resource "azurerm_storage_account" "postfixsa" {
  name                = "randomsa"
  resource_group_name = "${azurerm_resource_group.rg.name}"
  location            = "${azurerm_resource_group.rg.location}"
  account_tier        = "Standard"

  account_replication_type = "LRS"
}

# Create storage share for jupyterhub
resource "azurerm_storage_share" "postfixshare" {
  name = "postfix"

  resource_group_name  = "${azurerm_resource_group.rg.name}"
  storage_account_name = "${azurerm_storage_account.postfixsa.name}"

  quota = 10
}

# resource "null_resource" "certs" {
#   provisioner "local-exec" {
#          command = ""
#     }
# }


# Create container jupyterhub
resource "azurerm_container_group" "postfix" {
  name                = "postfix"
  location            = "${azurerm_resource_group.rg.location}"
  resource_group_name = "${azurerm_resource_group.rg.name}"
  ip_address_type     = "public"
  dns_name_label      = "${var.dnsname}"
  os_type             = "linux"

  container {
    name   = "testpostfix"
    image  = "catatnight/postfix"
    cpu    ="0.5"
    memory =  "0.5"
    port   = "587"

    environment_variables {
        maildomain = "${var.dnsname}.${azurerm_resource_group.rg.location}.azurecontainer.io"
        smtp_user = "testuser:Passw0rd"
    }

    volume {
      name       = "postfix"
      mount_path = "/etc/postfix/certs"
      read_only  = false
      share_name = "${azurerm_storage_share.postfixshare.name}"

      storage_account_name  = "${azurerm_storage_account.postfixsa.name}"
      storage_account_key   = "${azurerm_storage_account.postfixsa.primary_access_key}"
    }
  }

  tags {
    environment = "Test postfix"
  }
}